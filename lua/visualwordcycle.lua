local M = {}
---@class SelectionRange
---@field top_line number
---@field top_col number
---@field bot_line number
---@field bot_col number

---@return SelectionRange
local function get_visual_range()
	vim.cmd([[ execute "normal! \<ESC>" ]])
	local start_pos = vim.fn.getcharpos("'<")
	local end_pos = vim.fn.getcharpos("'>")
	vim.cmd([[ execute "normal! gv" ]])
	return {
		top_line = start_pos[2],
		top_col = start_pos[3],
		bot_line = end_pos[2],
		bot_col = end_pos[3],
	}
end

---@type SelectionRange
local _range = nil
---@type table<string>
local _words = nil
---@type number
local _word_index = 0

-- this is a test
-- please be good

---@param range SelectionRange
local function has_changed(range)
	-- vim.print("cur range:")
	-- vim.print(range)
	-- vim.print("old range:")
	-- vim.print(_range)
	for k, v in pairs(_range) do
		if range[k] ~= v then
			return true
		end
	end
end

local function register_selection_words()
	local curr_range = get_visual_range()
	-- vim.print(curr_range)
	if _range == nil or has_changed(curr_range) then
		_range = curr_range
		_words = {}
		_word_index = 0
	end
	if _range == nil then
		vim.print("error: _range is nil")
		return
	end
	local lines = vim.api.nvim_buf_get_lines(0, curr_range.top_line - 1, curr_range.bot_line, false)
	for _, line in ipairs(lines) do
		-- for w in line:gmatch("%w+") do
		-- 	table.insert(_words, w)
		-- end
		for w in line:gmatch("%w+") do
			table.insert(_words, w)
		end
	end
end

---@param next boolean
local function select_word_index(next)
	if next then
		_word_index = _word_index and _word_index + 1 or 1
	else
		_word_index = _word_index and _word_index - 1 or #_words
	end
	if _word_index > #_words then
		_word_index = 1
	end
	if _word_index < 1 then
		_word_index = #_words
	end
end

local function search_cycle()
	if _word_index then
		local word = _words[_word_index]
		vim.print(word)
		if word then
			-- vim.fn.setreg("/", "\\<" .. word .. "\\>")
			vim.fn.setreg("/", word)
		end
	end
end

---@param next boolean
local function cycle_words(next)
	register_selection_words()
	select_word_index(next)
	search_cycle()
end

local function cursor_word()
	local word = vim.fn.expand("<cword>")
	vim.fn.setreg("/", "\\<" .. word .. "\\>")
end

local function set_keymaps()
	local map = vim.keymap.set
	map("x", "<leader>n", function()
		cycle_words(true)
	end, { desc = "cycle search next word" })
	map("x", "<leader>p", function()
		cycle_words(false)
	end, { desc = "cycle search previous word" })
	-- map("x", "<leader>wp", function()
	-- 	vim.print(_words)
	-- end, { desc = "show selection words" })
	map("x", "<leader><c-w>", function()
		cursor_word()
	end, { desc = "show selection words" })
end

function M.setup()
	set_keymaps()
end

return M
